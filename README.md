# docker-atlassian-sdk

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/e3dcfac97a1c42f8b3671a1e4a8454ed)](https://www.codacy.com/app/Nebulio/docker-atlassian-sdk?utm_source=nebulio@bitbucket.org&amp;utm_medium=referral&amp;utm_content=nebulio/docker-atlassian-sdk&amp;utm_campaign=Badge_Grade)

Docker image containing 
+   Atlassian SDK
+   git
+   Codacy jar
+   AWS Tools
+   Python
+   Node JS