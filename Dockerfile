FROM openjdk:8-jdk

WORKDIR /opt
 
RUN sh -c 'echo "deb https://packages.atlassian.com/debian/atlassian-sdk-deb/ stable contrib" >>/etc/apt/sources.list' && \
    wget https://packages.atlassian.com/api/gpg/key/public && \
    apt-key add public && \
    apt-get update && apt-get install -y atlassian-plugin-sdk && \
    #curl -O "https://sdkrepo.atlassian.com/deb-archive/atlassian-plugin-sdk_6.3.12_all.deb" && \
    #dpkg -i "atlassian-plugin-sdk_6.3.12_all.deb" && \
    #apt-get install -y --no-install-recommends -f atlassian-plugin-sdk=6.3.12 && \
    #apt-get update  && \
    #apt-get install -y --no-install-recommends apt-transport-https=1.4.8 && \
    apt-get install -y --no-install-recommends apt-transport-https && \
    curl -sL "https://deb.nodesource.com/setup_16.x" | bash -  && \
    apt-get install -y git nodejs jq && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*  && \
    #curl "https://bootstrap.pypa.io/get-pip.py" | python3 -  && \
    #pip install awscli==1.16.67 && \
    curl -L -o codacy-coverage-reporter-assembly-latest.jar "$(curl -L 'https://api.github.com/repos/codacy/codacy-coverage-reporter/releases/latest' | jq -r '.assets[0].browser_download_url')"

CMD atlas-version && node -v